# Bibliography

This repository contains the personal bibliography of Marek Felšöci in the
LaTeX bibliography file format. See the [personal.bib](personal.bib) file.